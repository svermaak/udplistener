﻿Imports System.Threading
Imports System.Net.Sockets
Imports System.Net
Imports System.Text

Public Class Service
    Private Shared objServiceThread As Thread
    Dim blnKeepRunning As Boolean = True
    Public Sub UDPListnerThread()
        Dim intListenPort As Integer = 11000
        Dim objListener As New UdpClient(intListenPort)
        Dim objGroupEP As New IPEndPoint(IPAddress.Any, intListenPort)
        Dim strReceivedData As String
        Dim bytReceiveByteArray As Byte()
        Try
            While blnKeepRunning
                'Waiting for broadcast
                bytReceiveByteArray = objListener.Receive(objGroupEP)
                'Received a broadcast
                strReceivedData = Encoding.ASCII.GetString(bytReceiveByteArray, 0, bytReceiveByteArray.Length)

                'Write message to file
                Dim objFile As System.IO.StreamWriter
                objFile = My.Computer.FileSystem.OpenTextFileWriter("C:\Temp\Message_" & System.Guid.NewGuid.ToString() & ".txt", False)
                objFile.WriteLine(strReceivedData)
                objFile.Close()
            End While
        Catch e As Exception
            'Error
        End Try
        objListener.Close()
    End Sub
    Protected Overrides Sub OnStart(ByVal args() As String)
        'Start UDP Listner Thread
        blnKeepRunning = True
        objServiceThread = New Thread(AddressOf UDPListnerThread)
        objServiceThread.Name = "UDP Listner Thread"
        objServiceThread.Start()
    End Sub

    Protected Overrides Sub OnStop()
        'Stop UDP Listner Thread
        blnKeepRunning = False
        objServiceThread.Abort()
        objServiceThread = Nothing
    End Sub
End Class
